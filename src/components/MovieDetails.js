import React, { useState, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import axios from 'axios'

const MovieDetails = () => {
    const id = useParams();
    const [data, setData] = useState([]);

    useEffect(() => {
        fetchMovie();
    }, []);

    const fetchMovie = () => {
        axios
            .get(
                `http://www.omdbapi.com/?i=${id.movieId}&apikey=d7a26a70`
            )
            .then((res) => {
                setData(res.data);
            })
            .catch((err) => console.log(err));
    };


    return (
        <div>
            <div className="movie-container" key={data.imdbID}>
                <div className="movie-img">
                    <img src={data.Poster} alt='' />
                </div>

                <div className="movie-content">
                    <h1>{data.Title}</h1>
                    <p>Description: {data.Plot}</p>
                    <a
                        href={'https://www.imdb.com/title/' + id.movieId}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="btn"
                        style={{ textDecoration: 'none' }}
                    >
                        View on IMDB
                    </a>
                </div>
            </div>
            <Link to="/" className="btn">
                Go Back To Movies
            </Link>
           
        </div>

    );

}

export default MovieDetails