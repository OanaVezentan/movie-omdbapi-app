import React, { useState, useEffect } from 'react'

import MovieList from './MovieList';

const MovieOverview = () => {
    const [movies, setMovies] = useState([]);
    
    const getMovieRequest = async () => {
        const url = `http://www.omdbapi.com/?s=superhero&apikey=d7a26a70`;

        const response = await fetch(url);
        const responseJson = await response.json();

        if (responseJson.Search) {
            setMovies(responseJson.Search);
        }
    };

    useEffect(() => {
        getMovieRequest();
    }, []);

    return (
        <div>
            <MovieList movies={movies} />
        </div>
    );
};

export default MovieOverview;