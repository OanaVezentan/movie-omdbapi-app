import React from 'react'
import { Link } from 'react-router-dom'

const MovieList = (props) => {

    const movies = props.movies.map(movie => {
        return (
            <div key={movie.imdbID} className="flex-container">
                <div className="col">
                    <Link to={`/${movie.imdbID}`}  style={{ float: 'left'}}><img src={movie.Poster} alt="Movie Cover" /></Link>
                </div>
            </div>
        );
    });

    return (
        <>
            <h1>Movies Page</h1>
            {movies}
        </>
    );
};

export default MovieList;