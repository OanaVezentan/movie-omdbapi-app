import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.scss' 

import MovieOverview from "./components/MovieOverview";
import MovieDetails from "./components/MovieDetails";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/">
            <MovieOverview />
          </Route>
          <Route path="/:movieId">
            <MovieDetails />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
